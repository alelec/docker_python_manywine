FROM ubuntu:latest
MAINTAINER https://gitlab.com/alelec/docker-arm-none-eabi

RUN apt update && apt install -y wget xvfb xauth software-properties-common apt-transport-https

# Install wine https://www.winehq.org/download
RUN dpkg --add-architecture i386 && \
    wget https://dl.winehq.org/wine-builds/Release.key && \
    apt-key add Release.key && \
    apt-add-repository https://dl.winehq.org/wine-builds/ubuntu/ && \
    apt update && apt install -y winehq-stable

RUN wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks && \
    chmod +x /winetricks && mv /winetricks /usr/bin

RUN adduser --home /home/wine \
			--disabled-password \
			--shell /bin/bash \
			--quiet --system \
			--gecos "Wine Docker" \
			wine

USER wine

#ENV WINEPREFIX=~/.wine
#RUN wine wineboot

RUN xvfb-run -a winetricks -q comctl32 
RUN xvfb-run -a winetricks -q vcrun2013 
RUN xvfb-run -a winetricks -q vcrun2010 
RUN xvfb-run -a winetricks -q vcrun2008

RUN wget https://github.com/AdaCore/gsh/releases/download/gsh-0.1/gsh-0.1.tar.gz && \
    cd /$WINEPREFIX/drive_c/ && tar xvf /gsh-0.1.tar.gz 

ENV WINEPATH="c:\\gsh-0.1\\bin"

#ENTRYPOINT wine "c:\\gsh-0.1\\bin\\gsh"
ENTRYPOINT wine cmd



